import 'dart:io';

//import 'package:camera/camera.dart';
import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';

class Cameraview extends StatefulWidget {
  @override
  _CameraviewState createState() => _CameraviewState();
}


class _CameraviewState extends State<Cameraview> {

  CameraController controllerCam;

  List<CameraDescription> cameras;

  String filepath = "";

  //init camera
  void initCamera () async{
    cameras = await availableCameras();
    controllerCam = CameraController(cameras[0], ResolutionPreset.high);
    controllerCam.initialize().then((_){
      if(!mounted){
        return;
      }

      setState(() {

      });
    });
  }

  void _showCameraException(CameraException e) {
    String errorText = 'Error: ${e.code}\nError Message: ${e.description}';
    print(errorText);
  }

  //take picture
  Future _takePicture() async {
    if (!controllerCam.value.isInitialized) {

      return null;
    }

    // Do nothing if a capture is on progress
    if (controllerCam.value.isTakingPicture) {
      return null;
    }

    final Directory appDirectory = await getApplicationDocumentsDirectory();
    final String pictureDirectory = '${appDirectory.path}/Pictures';
    await Directory(pictureDirectory).create(recursive: true);

    final String currentTime = DateTime.now().millisecondsSinceEpoch.toString();
    final String filePath = '$pictureDirectory/${currentTime}.jpg';

    try {
      await controllerCam.takePicture(filePath);
    } on CameraException catch (e) {
      _showCameraException(e);
      return null;
    }

    return filePath;
  }

  @override
  void initState() {
    super.initState();
    initCamera();
  }

  @override
  void dispose() {
    controllerCam?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (!controllerCam.value.isInitialized) {
      return Container();
    }
    return Scaffold(
      appBar: AppBar(
        title: Text("Ambil Gambar"),
      ),
      body: Container(
       child: Stack(
         children: <Widget>[
           Container(
             height: 100,
             margin: EdgeInsets.only(top: 16),
             child: SingleChildScrollView(
               child: Column(
                 mainAxisAlignment: MainAxisAlignment.center,
                 children: <Widget>[
                   Container(
                     margin: EdgeInsets.only(left: 16, right: 16),
                     child: Center(
                       child: AspectRatio(
                         aspectRatio:
//                         1/controllerCam.value.aspectRatio,
                         16/9,
                         child: CameraPreview(controllerCam),),
                     ),
                   ),
                 ],
               ),
             ),
           ),
           Container(
             width: double.infinity,
             height: 350,
             margin: EdgeInsets.only(top : 130, bottom: 100),
             child: filepath == "" ? null : Image.file(File(filepath)),
           )
         ],
       ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.check),
        onPressed: () async{
          var filepathpict = await _takePicture();
          setState(() {
            filepath = filepathpict;
          });
        },
      ),
    );
  }
}
