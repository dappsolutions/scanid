import 'dart:io';

//import 'package:camera/camera.dart';
//import 'package:camera_fix_exception/camera.dart';
import 'package:camera/camera.dart';
import 'package:firebase_ml_vision/firebase_ml_vision.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:scanid/scan/controllers/AttachFileBloc.dart';
import 'package:scanid/scan/controllers/VerifikasiBloc.dart';
import 'package:scanid/scan/events/CameraEvent.dart';
import 'package:scanid/scan/events/ScanEvents.dart';
import 'package:scanid/scan/models/ScanModels.dart';
import 'package:scanid/scan/views/cameraview.dart';




class ScanView extends StatefulWidget {
  @override
  _ScanViewState createState() => _ScanViewState();
}

class _ScanViewState extends State<ScanView> {

  final _attachFile = AttachFileBloc();
  final _verifikasi = VerifikasiBloc();

  CameraController controllerCam;

  List<CameraDescription> cameras;

  String filepath = "";

  //init camera
//  void initCamera () async{
//    cameras = await availableCameras();
//    controllerCam = CameraController(cameras[0], ResolutionPreset.high);
//    controllerCam.initialize().then((_){
//      if(!mounted){
//        return;
//      }
//
//      setState(() {
//
//      });
//    });
//  }

  @override
  void initState() {
    _attachFile.cameraEventSink.add(initCamera());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Scan Id Card"),
      ),
      body: SingleChildScrollView(
        child: Stack(
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(left:16, right: 16, top: 16),
              child: Center(
                child: Text("SCAN STNK DISINI"),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 40),
              height: 400,
              child: SingleChildScrollView(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(left: 16, right: 16),
                      child: Center(
                        child: StreamBuilder(
                            stream: _attachFile.cameraStream,
                            builder: (context, AsyncSnapshot<CameraController> snapshot){
                              if(snapshot.data != null){
                                return Container(
                                  child: Column(
                                    children: <Widget>[
                                      AspectRatio(
                                      aspectRatio:
                                      16/7,
                                          child: CameraPreview(snapshot.data)
                                      ),

                                    ],
                                  ),
                                );
                              }
//
                              return CircularProgressIndicator();
                            },
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),


//            Container(
//              margin: EdgeInsets.only(top : 100),
//              child: Container(
//                alignment: Alignment.topRight,
//                margin: EdgeInsets.only(right: 16),
//                child: FloatingActionButton(
//                  backgroundColor: Colors.red,
//                  child: Icon(Icons.check),
//                  onPressed: () => _attachFile.scanEventSink.add(takePictureEvent()),
//                ),
//              ),
//            ),

            StreamBuilder(
              stream: _attachFile.attachFileStream,
              builder: (context, AsyncSnapshot<File> snapshot){
                return Container(
                  child: snapshot.data == null
                      ? Container(
                    margin: EdgeInsets.only(left: 16, right: 16, top: 330),
                    child: Center(
                      child: Text('Tidak ada gambar ditangkap'),
                    ),
                  )
                      : Column(
                    children: <Widget>[
                      SizedBox(
                        height: 50,
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 16, right: 16, top: 120),
                        child: Text('HASIL OUTPUT'),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 16, right: 16),
                        child: Image.file(snapshot.data, height: 300, width: double.infinity,fit: BoxFit.fill,),
                      ),
                      SizedBox(
                        height: 10,
                      ),
//                      Container(
//                        margin: EdgeInsets.only(left: 16, right: 16),
//                        width: double.infinity,
//                        height: 50,
//                        child: RaisedButton(
//                          child: Text("ULANGI", style: TextStyle(color: Colors.white70),),
//                          color: Colors.orange,
//                          onPressed: () => _attachFile.cameraEventSink.add(initCamera()),
//                        ),
//                      ),
//                      SizedBox(
//                        height: 10,
//                      ),
//                  RaisedButton(
//                    onPressed: () => _verifikasi.scanEventSink.add(VerifikasiDataEvent(snapshot.data)),
//                    child: Text("Verifikasi Data", style: TextStyle(color: Colors.white),),
//                    color: Colors.orange,
//                  ),
                      StreamBuilder(
                        stream: _attachFile.verifyDataStream,
                        builder: (context, AsyncSnapshot<List<ScanModels>> snapshot){
                          return Container(
                            padding: EdgeInsets.all(16),
                            margin: EdgeInsets.only(left: 16, right: 16),
                            child: snapshot.data == null ? CircularProgressIndicator() : ListView.builder(
                              padding: EdgeInsets.all(16),
                              shrinkWrap: true,
                              itemCount: snapshot.data.length,
                              itemBuilder: (context, pos){
                                return Container(
                                  height: 60,
                                  child: RaisedButton(
                                    child: Text("Sukses Terkirim : ("+snapshot.data[pos].text+")",),
                                    onPressed: () async{
                                      await _verifikasi.kirimToDE(snapshot.data[pos].text);
                                      showDialog(
                                          context: context,
                                          builder: (context){
                                            return AlertDialog(
                                              title: Text("Info"),
                                              content: Text("Sukses Terkirim"),
                                            );
                                          }
                                      );
                                    },
                                    shape: RoundedRectangleBorder(
                                        borderRadius: new BorderRadius.circular(18.0),
                                        side: BorderSide(color: Colors.orange)
                                    ),
                                  ),
                                );
                              },
                            ),
                          );
                        },
                      )

                    ],
                  ),
                );
              },
            )
          ],
        ),
      ),
      floatingActionButton:   FloatingActionButton(
        child: Icon(Icons.camera_alt),
        backgroundColor: Colors.orange,
//        onPressed: () => _attachFile.scanEventSink.add(attachFileEvent()),
        onPressed: () => _attachFile.scanEventSink.add(takePictureEvent()),
//        onPressed: (){
//          Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => new Cameraview()));
//        }
      ),
//      floatingActionButton: Column(
//        children: <Widget>[
//          SizedBox(
//            height: 580,
//          ),
//          FloatingActionButton(
//            child: Icon(Icons.camera_alt),
//            backgroundColor: Colors.orange,
//            onPressed: () => _attachFile.scanEventSink.add(attachFileEvent()),
//          ),
//          SizedBox(
//            height: 8,
//          ),
//          FloatingActionButton(
//            child: Icon(Icons.insert_drive_file),
//            backgroundColor: Colors.orange,
//            onPressed: () => _attachFile.scanEventSink.add(newEvent()),
//          )
//        ],
//      ),
    );
  }
}
