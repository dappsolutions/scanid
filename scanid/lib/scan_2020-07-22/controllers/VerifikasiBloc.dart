import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:firebase_ml_vision/firebase_ml_vision.dart';
import 'package:scanid/scan/events/ScanEvents.dart';
import 'package:scanid/scan/models/ScanModels.dart';
import 'package:http/http.dart' as http;
import 'package:scanid/utils/ApiService.dart';

class VerifikasiBloc {
  String word = "";
  List<ScanModels> dataScanned;


  final _verifikasiDataStateController = StreamController<List<ScanModels>>();

  StreamSink<List<ScanModels>> get _verifikasiData => _verifikasiDataStateController.sink;

  Stream<List<ScanModels>> get verifikasiDataStream =>
      _verifikasiDataStateController.stream;
  final _scanEventController = StreamController<VerifikasiEvent>();

  Sink<VerifikasiEvent> get scanEventSink => _scanEventController.sink;

  VerifikasiBloc() {
    _scanEventController.stream.listen(_mapEventToState);
  }

  void _mapEventToState(VerifikasiEvent event) async {
    List<ScanModels> dataResult = [];
    if (event is VerifikasiDataEvent) {
      File _image = event.image;
      FirebaseVisionImage ourImage = FirebaseVisionImage.fromFile(_image);
      TextRecognizer recognizeText = FirebaseVision.instance.textRecognizer();
      VisionText readText = await recognizeText.processImage(ourImage);

      var wordText = StringBuffer();
      for (TextBlock block in readText.blocks) {
        for (TextLine line in block.lines) {
          dataResult.add(ScanModels(
              text: line.text.toString().trim()
          ));
//          for (TextElement word in line.elements) {
//            wordText.write(word.text + " [&&] ");
//
//            dataResult.add(ScanModels(
//              text: word.text.toString().trim()
//            ));
//          }
        }
      }

    }

    _verifikasiData.add(dataResult);
  }

  void dispose() {
    _verifikasiDataStateController.close();
    _scanEventController.close();
  }

  String getModule(){
    return "probe/kendaraanmasuk";
  }

  void kirimToDE(String text) async {
    var url = ApiService.getBaseUrl() + getModule()+"/createFileDE";
    print("url "+url);
    Map params = Map<String, String>();
    params["text"] = text;
    params["jenis"] = "stnk";

    var data_api = await http.post(url, body: params);
//    print("data_api "+data_api.body);
    switch(data_api.statusCode){
      case 200:
//        var _data_json = jsonDecode(data_api.body);

        break;
      case 404:
        print("Halaman Tidak Ditemukan "+url.toString());
        print("url "+url);
        break;
      case 500:
        print("Internal Server Error");
        break;
    }
  }
}
