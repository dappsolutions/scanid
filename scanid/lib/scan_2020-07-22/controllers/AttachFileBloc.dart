import 'dart:async';
import 'dart:io';
import 'package:camera/camera.dart';
import 'package:firebase_ml_vision/firebase_ml_vision.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path_provider/path_provider.dart';
import 'package:scanid/scan/events/CameraEvent.dart';
import 'package:scanid/scan/events/ScanEvents.dart';
import 'package:scanid/scan/models/ScanModels.dart';
import 'package:scanid/utils/ApiService.dart';
import 'package:http/http.dart' as http;



class AttachFileBloc {
  File image;

  final _attachFileStateController = StreamController<File>();
  StreamSink<File> get _attachFile => _attachFileStateController.sink;
  Stream<File> get attachFileStream => _attachFileStateController.stream;
  final _scanEventController = StreamController<scanEvents>();
  Sink<scanEvents> get scanEventSink => _scanEventController.sink;

  final _verifyFileStateController = StreamController<List<ScanModels>>();
  StreamSink<List<ScanModels>> get _verifyFile => _verifyFileStateController.sink;
  Stream<List<ScanModels>> get verifyDataStream =>
      _verifyFileStateController.stream;

  final _verifyEventController = StreamController<VerifikasiEvent>();

  Sink<VerifikasiEvent> get verifyEventSink => _verifyEventController.sink;

  //stream and sink camera
  final _cameraStateController = StreamController<CameraController>();
  StreamSink<CameraController> get _cameraEvent => _cameraStateController.sink;
  Stream<CameraController> get cameraStream => _cameraStateController.stream;
  final _cameraEventController = StreamController<CameraEvent>();
  Sink<CameraEvent> get cameraEventSink => _cameraEventController.sink;


  CameraController controller_cam;
  String filepath = "";

  AttachFileBloc(){
    _scanEventController.stream.listen(_mapEventToState);
    _verifyEventController.stream.listen(verifidata);
    _cameraEventController.stream.listen(cameraStreamEvent);
  }

  void _mapEventToState(scanEvents event) async {
    if(event is attachFileEvent){
      print("masuk");
//      var imageAttach = await ImagePicker.pickImage(source: ImageSource.gallery);
      var imageAttach = await ImagePicker.pickImage(source: ImageSource.camera);
      var imgCropper = await cropFile(imageAttach);
      image = imgCropper;

      verifyEventSink.add(VerifikasiDataEvent(imgCropper));
    }

    if(event is takePictureEvent){
      var controllerCam = controller_cam;
      if (!controllerCam.value.isInitialized) {
        return;
      }
      // Do nothing if a capture is on progress
      if (controllerCam.value.isTakingPicture) {
        return;
      }

      final Directory appDirectory = await getApplicationDocumentsDirectory();
      final String pictureDirectory = '${appDirectory.path}/Pictures';
      await Directory(pictureDirectory).create(recursive: true);

      final String currentTime = DateTime.now().millisecondsSinceEpoch.toString();
      final String filePath = '$pictureDirectory/${currentTime}.jpg';

      try {
        await controllerCam.takePicture(filePath);
      } on CameraException catch (e) {
        print(e);
        return;
      }

      File img = File(filePath);
      var imgCropper = await cropFile(img);
      image = imgCropper;

      verifyEventSink.add(VerifikasiDataEvent(imgCropper));
    }

    if(event is newEvent){
      File img;
      image = img;
    }
    
    _attachFile.add(image);
  }

  void cameraStreamEvent(CameraEvent event) async {
    CameraController controllerCam;

    List<CameraDescription> cameras;
    if(event is initCamera){
      cameras = await availableCameras();
      controllerCam = CameraController(cameras[0], ResolutionPreset.high);
      await controllerCam.initialize();
      controller_cam = controllerCam;

      scanEventSink.add(newEvent());
    }

    _cameraEvent.add(controller_cam);
  }

  void verifidata(VerifikasiEvent event) async {
    List<ScanModels> dataResult = [];
    if (event is VerifikasiDataEvent) {
      File _image = event.image;
      FirebaseVisionImage ourImage = FirebaseVisionImage.fromFile(_image);
      TextRecognizer recognizeText = FirebaseVision.instance.textRecognizer();
      VisionText readText = await recognizeText.processImage(ourImage);

      var wordText = StringBuffer();
      for (TextBlock block in readText.blocks) {
        for (TextLine line in block.lines) {
          dataResult.add(ScanModels(
              text: line.text.toString().trim()
          ));
//          for (TextElement word in line.elements) {
//            wordText.write(word.text + " [&&] ");
//
//            dataResult.add(ScanModels(
//              text: word.text.toString().trim()
//            ));
//          }
        }
      }
    }

    _verifyFile.add(dataResult);
  }




  Future<File> cropFile(File imageSource) async {
    File cropImg = await ImageCropper.cropImage(
        sourcePath: imageSource.path,
      aspectRatioPresets: [
        CropAspectRatioPreset.square,
        CropAspectRatioPreset.ratio3x2,
        CropAspectRatioPreset.original,
        CropAspectRatioPreset.ratio4x3,
        CropAspectRatioPreset.ratio16x9
      ],
      androidUiSettings: AndroidUiSettings(
        toolbarTitle: 'Crop Image',
        toolbarColor: Colors.deepOrange,
        toolbarWidgetColor: Colors.white,
        initAspectRatio: CropAspectRatioPreset.original,
        lockAspectRatio: false
      )
    );

    return cropImg;
  }

  void dispose(){
    _attachFileStateController.close();
    _scanEventController.close();
    _verifyFileStateController.close();
    _verifyEventController.close();
  }


  String getModule(){
    return "probe/kendaraanmasuk";
  }

  void kirimToDE(String text) async {
    var url = ApiService.getBaseUrl() + getModule()+"/createFileDE";
    print("url "+url);
    Map params = Map<String, String>();
    params["text"] = text;
    params["jenis"] = "stnk";

    var data_api = await http.post(url, body: params);
//    print("data_api "+data_api.body);
    switch(data_api.statusCode){
      case 200:
//        var _data_json = jsonDecode(data_api.body);

        break;
      case 404:
        print("Halaman Tidak Ditemukan "+url.toString());
        print("url "+url);
        break;
      case 500:
        print("Internal Server Error");
        break;
    }
  }


}