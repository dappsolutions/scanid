
import 'dart:io';

abstract class scanEvents{}

class attachFileEvent extends scanEvents{}

class takePictureEvent extends scanEvents{}

class newEvent extends scanEvents{}

abstract class VerifikasiEvent{
  final File image;
  VerifikasiEvent(this.image);
}

class VerifikasiDataEvent extends VerifikasiEvent{
  VerifikasiDataEvent(File image) : super(image);
}