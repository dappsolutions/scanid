
import 'dart:async';
import 'dart:convert';

import 'package:scanid/login/events/LoginEvents.dart';
import 'package:scanid/login/models/UserModels.dart';
import 'package:http/http.dart' as http;
import 'package:scanid/utils/ApiService.dart';

class LoginBloc{
    List<UserModels> data;

    //Proses Scan Barcode
    final _LoginStateController = StreamController<String>();

    StreamSink<String> get _Login => _LoginStateController.sink;

    Stream<String> get LoginStream => _LoginStateController.stream;

    final _LoginEventController = StreamController<LoginEvents>();

    Sink<LoginEvents> get LoginEventSink =>
        _LoginEventController.sink;

    LoginBloc() {
      _LoginEventController.stream.listen(_mapEventToState);
    }

  void _mapEventToState(LoginEvents event) async {
      if(event is ProsesLoginEvent){

      }
  }

  String getModule(){
//    return "user";
    return "present";
  }

  Future<List<UserModels>> prosesLoginMobile({String username, String password}) async{
    List<UserModels> data_result = [];
    var url = ApiService.getBaseUrl() + getModule()+"/prosesLoginMobile";
    Map params = Map<String, String>();
    params["username"] = username;
    params["password"] = password;
    var request = await http.post(url, body: params);

    switch(request.statusCode){
      case 200:
        var _data_json = jsonDecode(request.body);
        List data_sample = _data_json["data"];

        if (data_sample.length > 0) {
          for (var val in data_sample) {
            data_result.add(UserModels(
                username: val["username"]
            ));
          }
        }
        break;
      case 404:
        print("Halaman Tidak Ditemukan "+url.toString());
        print("url "+url);
        break;
      case 500:
        print("Internal Server Error");
        break;
    }

    return data_result;
  }
}