import 'package:flutter/material.dart';
import 'package:scanid/login/controllers/LoginBloc.dart';
import 'package:scanid/login/models/UserModels.dart';
import 'package:scanid/serahterima/views/SerahTerimaView.dart';

class LoginViews extends StatefulWidget {
  @override
  _LoginViewsState createState() => _LoginViewsState();
}

class _LoginViewsState extends State<LoginViews> {

  String user = "";
  String pass = "";

  final _LoginBloc = LoginBloc();


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("LOGIN"),
      ),
      body: SingleChildScrollView(
        child: Container(
            padding: EdgeInsets.all(24),
            child: Center(
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height: 30,
                  ),
                  Text(
                    "SERAH TERIMA",
                    style: TextStyle(fontSize: 18, color: Colors.grey),
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  TextField(
                    autofocus: true,
                    decoration: InputDecoration(
                        labelText: "Username",
                        hintText: "Username",
                        border: OutlineInputBorder(),
                        contentPadding: EdgeInsets.all(16),
                        suffixIcon: Icon(Icons.verified_user)),
                    onChanged: (text) {
                        user = text;
                    },
//                      controller: userTextController,
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  TextField(
                    decoration: InputDecoration(
                        labelText: "Password",
                        hintText: "Input Password",
                        border: OutlineInputBorder(),
                        contentPadding: EdgeInsets.all(16),
//                          prefixIcon: Icon(Icons.lock),
                        suffixIcon: IconButton(
                          icon: Icon(Icons.visibility),
                          onPressed: (){

                          },
                        )
                    ),
                    onChanged: (text) {
                      pass = text;
                    },

//                      controller: passTextController,
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  ButtonTheme(
                    minWidth: double.infinity,
                    child: RaisedButton(
//                      onPressed: () async {
//                        Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => new SerahTerimaView()));
//                      },
                      onPressed: () async{

                        showDialog(context: context,
                        builder: (context){
                          return SizedBox(
                              height: 30.0,
                              width: 30.0,
                              child:
                              CircularProgressIndicator(
                                  valueColor: AlwaysStoppedAnimation(Colors.blue),
                                  strokeWidth: 5.0)
                          );
                        });

                        await _LoginBloc.prosesLoginMobile(username : user, password: pass).then((data){
                          var username = data[0].username;
                          print("username "+username);
                        }).catchError((error){
                            print(error.toString());
                        });

                        Navigator.pop(context);
//                        FutureBuilder(
//                          future: ,
//                          builder: (context, AsyncSnapshot<List<UserModels>> snap){
//                            print("sanap "+snap.toString());
//                            if(snap != null){
//                              return Text("okeee");
//                            }
//                            return Text("");
//                          },
//                        );
                      },
                      padding: EdgeInsets.all(16),
                      child: Text(
                        "Login",
                        style: TextStyle(color: Colors.white),
                      ),
                    )
                  ),
                ],
              ),
            )),
      ),
    );
  }
}
