
abstract class LoginEvents{
  final String username;
  final String password;

  LoginEvents({this.username, this.password});
}

class ProsesLoginEvent extends LoginEvents{}