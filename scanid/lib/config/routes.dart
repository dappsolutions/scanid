

import 'package:flutter/material.dart';
import 'package:scanid/serahterima/models/serahterima.models.dart';
import 'package:scanid/serahterima/views/SerahTerimaView.dart';

class Navigate {
  static Map route = {
    SerahTerimaModels.modules: {
      'main': '',
      'serah_terima': (BuildContext context, {String userFinger, String serahTerima}) {
        Navigator.push(context, MaterialPageRoute(builder: (context) => SerahTerimaView(
          userFinger: userFinger,
          serahTerima: serahTerima,
        )));
      },
    }
  };
}