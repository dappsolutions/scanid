import 'dart:io';

//import 'package:camera/camera.dart';
//import 'package:camera_fix_exception/camera.dart';
import 'package:camera/camera.dart';
import 'package:firebase_ml_vision/firebase_ml_vision.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_picker/image_picker.dart';
import 'package:scanid/scan/bloc/scan.bloc.dart';
import 'package:scanid/scan/events/ScanEvents.dart';
import 'package:scanid/scan/events/scan.event.dart';
import 'package:scanid/scan/models/ScanModels.dart';
import 'package:scanid/scan/state/scan.state.dart';
import 'package:scanid/scan/views/cameraview.dart';

class ScanView extends StatefulWidget {
  @override
  _ScanViewState createState() => _ScanViewState();
}

class _ScanViewState extends State<ScanView> {
  ScanCameraBloc _bloc;

  @override
  void initState() {
    _bloc = new ScanCameraBloc();

//    _bloc.add(RequestCamera());
    context.bloc<ScanCameraBloc>().add(RequestCamera());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Scan Id Card"),
      ),
      body: SingleChildScrollView(
        child: Stack(
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(left: 16, right: 16, top: 16),
              child: Center(
                child: Text("SCAN STNK DISINI"),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 40),
              height: 400,
                child: SingleChildScrollView(
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(left: 16, right: 16),
                          child: BlocBuilder<ScanCameraBloc, ScanState>(
//                              bloc: _bloc,
                              builder: (_, state){
                                if(state is SetCamera){
                                  return Container(
                                    child: Column(
                                      children: <Widget>[
                                        AspectRatio(
                                            // aspectRatio:
                                            // 16/7,
                                            child: Stack(
                                              fit: StackFit.expand,
                                              children: <Widget>[
                                                CameraPreview(state.cam),
                                                cameraOverlay(
                                                  padding: 50,
                                                  aspectRatio: 16/7,
                                                  color: Color(0x55000000)
                                                )
                                              ],
                                            ),
                                            aspectRatio: 0.8,
                                        ),

                                      ],
                                    ),
                                  );
                                }

                                return Container(
                                  child: Center(
                                    child: Text("Sedang Inisisalisasi Camera...."),
                                  ),
                                );
                              }
                          )
                        ),

                        SizedBox(height: 10,),

                        BlocBuilder<ScanCameraBloc, ScanState>(
//                          bloc: _bloc,
                          builder: (_, state){
                              if(state is SetImageFromTakePict){
                                return Container(
                                  margin: EdgeInsets.only(left: 16, right: 16),
                                  child: Image.file(state.image, height: 300, width: double.infinity,fit: BoxFit.fill,),
                                );
                              }

                              return Container(
                                child: Center(
                                  child: Text("Menunggu Hasil Gambar"),
                                ),
                              );
                          },
                        ),

                        SizedBox(height: 10,),
                      ],
                  ),
                )
            ),
            BlocBuilder<ScanCameraBloc, ScanState>(
//                bloc: _bloc,
                builder:(context, state){
                  if(state is SetImageFromTakePict){
                    return Container(
                      margin: EdgeInsets.only(left: 16, right: 16, top: 370),
                      child: SingleChildScrollView(
                        child: ListView.builder(
                          padding: EdgeInsets.all(16),
                          shrinkWrap: true,
                          itemCount: state.result_ocr.length,
                          itemBuilder: (context, pos){
                            return Container(
                              height: 60,
                              child: RaisedButton(
                                child: Text("Sukses Terkirim : ("+state.result_ocr[pos].text+")",),
                                onPressed: () async{
                                  await _bloc.add(KirimKeDeEvent(word: state.result_ocr[pos].text.toString()));
                                  showDialog(
                                      context: context,
                                      builder: (context){
                                        return AlertDialog(
                                          title: Text("Info"),
                                          content: Text("Sukses Terkirim"),
                                        );
                                      }
                                  );
                                },
                                shape: RoundedRectangleBorder(
                                    borderRadius: new BorderRadius.circular(8.0),
                                    side: BorderSide(color: Colors.orange)
                                ),
                              ),
                            );
                          },
                        ),
                      ),
                    );
                  }

                  return Container(
                    child: Center(
                      child: Text("Hasil OCR Gambar"),
                    ),
                  );
                }
            )
          ],
        ),
      ),
    floatingActionButton: Column(
        children: <Widget>[
          SizedBox(
            height: 580,
          ),
          FloatingActionButton(
            child: Icon(Icons.camera_alt),
            backgroundColor: Colors.orange,
            onPressed: () {
//              _bloc.add(TakePictureEvent());
              context.bloc<ScanCameraBloc>().add(TakePictureEvent());
            },
          ),
          SizedBox(
            height: 8,
          ),
          FloatingActionButton(
            child: Icon(Icons.insert_drive_file),
            backgroundColor: Colors.orange,
            onPressed: () async{
//              _bloc.add(RequestCamera());
              context.bloc<ScanCameraBloc>().add(RequestCamera());

              // String imagePath;
              // // Platform messages may fail, so we use a try/catch PlatformException.
              // try {
              //   imagePath = await EdgeDetection.detectEdge;
              // } on PlatformException {
              //   imagePath = 'Failed to get cropped image path.';
              // }
              //
              // print("IMAGE PATH ${imagePath}");
            },
          )
        ],
      ),
    );
  }


  Widget cameraOverlay({double padding, double aspectRatio, Color color}) {
    return LayoutBuilder(builder: (context, constraints) {
      double parentAspectRatio = constraints.maxWidth / constraints.maxHeight;
      double horizontalPadding;
      double verticalPadding;

      if (parentAspectRatio < aspectRatio) {
        horizontalPadding = padding;
        verticalPadding = (constraints.maxHeight -
            ((constraints.maxWidth - 2 * padding) / aspectRatio)) /
            2;
      } else {
        verticalPadding = padding;
        horizontalPadding = (constraints.maxWidth -
            ((constraints.maxHeight - 2 * padding) * aspectRatio)) /
            2;
      }
      return Stack(fit: StackFit.expand, children: [
        Align(
            alignment: Alignment.centerLeft,
            child: Container(width: horizontalPadding, color: color)),
        Align(
            alignment: Alignment.centerRight,
            child: Container(width: horizontalPadding, color: color)),
        Align(
            alignment: Alignment.topCenter,
            child: Container(
                margin: EdgeInsets.only(
                    left: horizontalPadding, right: horizontalPadding),
                height: verticalPadding,
                color: color)),
        Align(
            alignment: Alignment.bottomCenter,
            child: Container(
                margin: EdgeInsets.only(
                    left: horizontalPadding, right: horizontalPadding),
                height: verticalPadding,
                color: color)),
        Container(
          margin: EdgeInsets.symmetric(
              horizontal: horizontalPadding, vertical: verticalPadding),
          decoration: BoxDecoration(border: Border.all(color: Colors.cyan)),
        )
      ]);
    });
  }
}
