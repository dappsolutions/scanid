import 'package:equatable/equatable.dart';

abstract class ScanCameraEvent extends Equatable {
  const ScanCameraEvent();

  @override
  List<Object> get props => [];
}

class RequestCamera extends ScanCameraEvent {}

class TakePictureEvent extends ScanCameraEvent{}

class KirimKeDeEvent extends ScanCameraEvent{
  String word;
  KirimKeDeEvent({this.word});
}