import 'dart:io';

import 'package:camera/camera.dart';
import 'package:firebase_ml_vision/firebase_ml_vision.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:path_provider/path_provider.dart';
import 'package:scanid/config/socket.dart';
import 'package:scanid/config/url.dart';
import 'package:scanid/scan/events/scan.event.dart';
import 'package:scanid/scan/models/ScanModels.dart';
import 'package:scanid/scan/models/scan.model.dart';
import 'package:scanid/scan/state/scan.state.dart';
import 'package:scanid/utils/ApiService.dart';
import 'package:http/http.dart' as http;

class ScanCameraBloc extends Bloc<ScanCameraEvent, ScanState> {
  ScanCameraBloc() : super(InitData());
//  @override
//  ScanState get initialState => InitData();

  @override
  Stream<ScanState> mapEventToState(ScanCameraEvent event) async* {
    if(event is RequestCamera){
        List<CameraDescription> cameras;
        cameras = await availableCameras();
        ScanCameraModel.cam = CameraController(cameras[0], ResolutionPreset.high);
        await ScanCameraModel.cam.initialize();

        yield SetCamera(cam: ScanCameraModel.cam);
    }


    if(event is TakePictureEvent){
      if (!ScanCameraModel.cam.value.isInitialized) {
        return;
      }
      // Do nothing if a capture is on progress
      if (ScanCameraModel.cam.value.isTakingPicture) {
        return;
      }

      final Directory appDirectory = await getApplicationDocumentsDirectory();
      final String pictureDirectory = '${appDirectory.path}/Pictures';
      await Directory(pictureDirectory).create(recursive: true);

      final String currentTime = DateTime.now().millisecondsSinceEpoch.toString();
      final String filePath = '$pictureDirectory/${currentTime}.jpg';
      await ScanCameraModel.cam.takePicture(filePath);

      File img = File(filePath);
      File imgCropper = await cropFile(img);

      File _image = imgCropper;
      FirebaseVisionImage ourImage = FirebaseVisionImage.fromFile(_image);
      TextRecognizer recognizeText = FirebaseVision.instance.textRecognizer();
      VisionText readText = await recognizeText.processImage(ourImage);

//      var wordText = StringBuffer();
      ScanCameraModel.data.clear();
      for (TextBlock block in readText.blocks) {
        for (TextLine line in block.lines) {
          print(line.text.toString().trim());
          ScanCameraModel.data.add(ScanModels(
              text: line.text.toString().trim()
          ));
        }
      }

      yield SetImageFromTakePict(image: imgCropper, result_ocr: ScanCameraModel.data);
    }

    if(event is KirimKeDeEvent){
        // WebSocketConfig.init();
        var url = Url.websocketurl+"/verifikasi/pushnopol?nopol=${event.word}";

        var data_api = await http.get(url);
        switch(data_api.statusCode){
          case 200:
  //        var _data_json = jsonDecode(data_api.body);
            print("data_api ${data_api.body}");
            break;
          case 404:
            print("Halaman Tidak Ditemukan "+url.toString());
            print("url "+url);
            break;
          case 500:
            print("Internal Server Error");
            break;
        }
        // print("MASUK EVENT ${event.word}");
        // WebSocketConfig.socketIO.sendMessage("sendnopol", event.word);
    }

  }

  Future<File> cropFile(File imageSource) async {
    File cropImg = await ImageCropper.cropImage(
        sourcePath: imageSource.path,
        aspectRatioPresets: [
          CropAspectRatioPreset.square,
          CropAspectRatioPreset.ratio3x2,
          CropAspectRatioPreset.original,
          CropAspectRatioPreset.ratio4x3,
          CropAspectRatioPreset.ratio16x9
        ],
        androidUiSettings: AndroidUiSettings(
            toolbarTitle: 'Crop Image',
            toolbarColor: Colors.deepOrange,
            toolbarWidgetColor: Colors.white,
            initAspectRatio: CropAspectRatioPreset.original,
            lockAspectRatio: false
        )
    );

    return cropImg;
  }
}
