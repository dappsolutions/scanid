import 'dart:io';

import 'package:equatable/equatable.dart';
import 'package:camera/camera.dart';
import 'package:scanid/scan/models/ScanModels.dart';

abstract class ScanState extends Equatable {
  const ScanState();
}

class InitData extends ScanState {
  @override
  List<Object> get props => [];
}

class SetCamera extends ScanState {
  CameraController cam;
  SetCamera({this.cam});

  @override
  List<Object> get props => [cam];
}

class SetImageFromTakePict extends ScanState{
  File image;
  List<ScanModels> result_ocr;
  SetImageFromTakePict({this.image, this.result_ocr});

  @override
  List<Object> get props => [image, result_ocr];
}

class ResultOcrText extends ScanState{
  List<ScanModels> result;
  ResultOcrText({this.result});

  @override
  List<Object> get props => [result];
}