import 'package:flutter/material.dart';
import 'package:scanid/config/routes.dart';
import 'package:scanid/serahterima/controllers/CheckController.dart';
import 'package:scanid/serahterima/models/serahterima.models.dart';

import 'SerahTerimaView.dart';

class ListUserView extends StatefulWidget {
  @override
  _ListUserViewState createState() => _ListUserViewState();
}

class _ListUserViewState extends State<ListUserView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("User Serah Terima"),
        backgroundColor: Colors.orange,
      ),
      body:SingleChildScrollView(
        child: Container(
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 20,
              ),
              Center(
                child: Text("Serah Sample", style: TextStyle(fontSize: 24),),
              ),
              SizedBox(
                height: 20,
              ),
              Center(
                  child: Container(
                    width: 200,
                    height: 60,
                    child: RaisedButton(
                      onPressed: () async{
                        var check = await CheckController().checkConnecttion();
                        if(check == "success"){
                          Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => new SerahTerimaView(
                            userFinger: "cahya",
                            serahTerima: "serah",
                          )));
                        }else{
                          showDialodMessage(check);
                        }
                      },
                      child: Text("Cahya"),
                      color: Colors.white,
                      shape: RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(18.0),
                          side: BorderSide(color: Colors.orange)
                      ),
                    ),
                  )
              ),
              SizedBox(
                height: 20,
              ),
              Center(
                  child: Container(
                    width: 200,
                    height: 60,
                    child: RaisedButton(
                      onPressed: () async{
                        var check = await CheckController().checkConnecttion();
                        if(check == "success"){
                          Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => new SerahTerimaView(
                            userFinger: "deni",
                            serahTerima: "serah",
                          )));
                        }else{
                          showDialodMessage(check);
                        }
                      },
                      child: Text("DENI"),
                      color: Colors.white,
                      shape: RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(18.0),
                          side: BorderSide(color: Colors.orange)
                      ),
                    ),
                  )
              ),
              SizedBox(
                height: 20,
              ),
              Center(
                  child: Container(
                    width: 200,
                    height: 60,
                    child: RaisedButton(
                      onPressed: () async{
                        var check = await CheckController().checkConnecttion();
                        if(check == "success"){
                          Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => new SerahTerimaView(
                            userFinger: "faruq",
                            serahTerima: "serah",
                          )));
                        }else{
                          showDialodMessage(check);
                        }
                      },
                      child: Text("FARUQ"),
                      color: Colors.white,
                      shape: RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(18.0),
                          side: BorderSide(color: Colors.orange)
                      ),
                    ),
                  )
              ),
              SizedBox(
                height: 20,
              ),
              Center(
                  child: Container(
                    height: 60,
                    width: 200,
                    child: RaisedButton(
                      onPressed: () async{
                        var check = await CheckController().checkConnecttion();
                        if(check == "success"){
                          Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => new SerahTerimaView(
                            userFinger: "hadi",
                            serahTerima: "serah",
                          )));
                        }else{
                          showDialodMessage(check);
                        }
                      },
                      child: Text("HADI"),
                      color: Colors.white,
                      shape: RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(18.0),
                          side: BorderSide(color: Colors.orange)
                      ),
                    ),
                  )
              ),
              SizedBox(
                height: 20,
              ),
              Center(
                  child: Container(
                    height: 60,
                    width: 200,
                    child: RaisedButton(
                      onPressed: () async{
                        var check = await CheckController().checkConnecttion();
                        if(check == "success"){
                          Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => new SerahTerimaView(
                            userFinger: "herik",
                            serahTerima: "serah",
                          )));
                        }else{
                          showDialodMessage(check);
                        }
                      },
                      child: Text("HERIK"),
                      color: Colors.white,
                      shape: RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(18.0),
                          side: BorderSide(color: Colors.orange)
                      ),
                    ),
                  )
              ),
              SizedBox(
                height: 20,
              ),
              Center(
                  child: Container(
                    height: 60,
                    width: 200,
                    child: RaisedButton(
                      onPressed: () async{
                        var check = await CheckController().checkConnecttion();
                        if(check == "success"){
                          Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => new SerahTerimaView(
                            userFinger: "liyan",
                            serahTerima: "serah",
                          )));
                        }else{
                          showDialodMessage(check);
                        }
                      },
                      child: Text("LIYAN"),
                      color: Colors.white,
                      shape: RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(18.0),
                          side: BorderSide(color: Colors.orange)
                      ),
                    ),
                  )
              ),
              SizedBox(
                height: 20,
              ),
              Center(
                  child: Container(
                    height: 60,
                    width: 200,
                    child: RaisedButton(
                      onPressed: () async{
                        var check = await CheckController().checkConnecttion();
                        if(check == "success"){
                          Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => new SerahTerimaView(
                            userFinger: "purwanto",
                            serahTerima: "serah",
                          )));
                        }else{
                          showDialodMessage(check);
                        }
                      },
                      child: Text("PURWANTO"),
                      color: Colors.white,
                      shape: RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(18.0),
                          side: BorderSide(color: Colors.orange)
                      ),
                    ),
                  )
              ),
              SizedBox(
                height: 20,
              ),
              Center(
                  child: Container(
                    height: 60,
                    width: 200,
                    child: RaisedButton(
                      onPressed: () async{
                        var check = await CheckController().checkConnecttion();
                        if(check == "success"){
                          Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => new SerahTerimaView(
                            userFinger: "wahib",
                            serahTerima: "serah",
                          )));
                        }else{
                          showDialodMessage(check);
                        }
                      },
                      child: Text("WAHIB"),
                      color: Colors.white,
                      shape: RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(18.0),
                          side: BorderSide(color: Colors.orange)
                      ),
                    ),
                  )
              ),
              SizedBox(
                height: 20,
              ),
              Divider(
                color: Colors.black,
              ),
              SizedBox(
                height: 10,
              ),
              Center(
                child: Text("Terima Sample", style: TextStyle(fontSize: 24),),
              ),
              SizedBox(
                height: 20,
              ),
              Center(
                child: Container(
                  height: 60,
                  width: 200,
                  child: RaisedButton(
                    onPressed: () async{
                      var check = await CheckController().checkConnecttion();
                      if(check == "success"){
                        Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => new SerahTerimaView(
                          userFinger: "fuad",
                          serahTerima: "terima",
                        )));
                      }else{
                        showDialodMessage(check);
                      }
                    },
                    child: Text("FUAD"),
                    color: Colors.white,
                    shape: RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(18.0),
                        side: BorderSide(color: Colors.green)
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Center(
                child: Container(
                  height: 60,
                  width: 200,
                  child: RaisedButton(
                    onPressed: () async{
                      var check = await CheckController().checkConnecttion();
                      if(check == "success"){
                        Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => new SerahTerimaView(
                          userFinger: "hendra",
                          serahTerima: "terima",
                        )));
                      }else{
                        showDialodMessage(check);
                      }
                    },
                    child: Text("HENDRA"),
                    color: Colors.white,
                    shape: RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(18.0),
                        side: BorderSide(color: Colors.green)
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Center(
                child: Container(
                  height: 60,
                  width: 200,
                  child: RaisedButton(
                    onPressed: () async{
                      var check = await CheckController().checkConnecttion();
                      if(check == "success"){
                        Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => new SerahTerimaView(
                          userFinger: "eri",
                          serahTerima: "terima",
                        )));
                      }else{
                        showDialodMessage(check);
                      }
                    },
                    child: Text("ERI"),
                    color: Colors.white,
                    shape: RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(18.0),
                        side: BorderSide(color: Colors.green)
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Center(
                  child: Container(
                      height: 60,
                      width: 200,
                      child: RaisedButton(
                        onPressed: () async{
                          var check = await CheckController().checkConnecttion();
                          if(check == "success"){
                            Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => new SerahTerimaView(
                              userFinger: "rozi",
                              serahTerima: "terima",
                            )));
                          }else{
                            showDialodMessage(check);
                          }
                        },
                        child: Text("ROZI"),
                        color: Colors.white,
                        shape: RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(18.0),
                            side: BorderSide(color: Colors.green)
                        ),
                      )
                  )
              ),
              SizedBox(
                height: 20,
              ),
              Center(
                  child: Container(
                      height: 60,
                      width: 200,
                      child: RaisedButton(
                        onPressed: () async{
                          var check = await CheckController().checkConnecttion();
                          if(check == "success"){
                            Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => new SerahTerimaView(
                              userFinger: "dimas",
                              serahTerima: "terima",
                            )));
                          }else{
                            showDialodMessage(check);
                          }
                        },
                        child: Text("DIMAS"),
                        color: Colors.white,
                        shape: RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(18.0),
                            side: BorderSide(color: Colors.green)
                        ),
                      )
                  )
              ),
              SizedBox(
                height: 20,
              ),
              Center(
                  child: Container(
                      height: 60,
                      width: 200,
                      child: RaisedButton(
                        onPressed: () async{
                          var check = await CheckController().checkConnecttion();
                          if(check == "success"){
                            Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => new SerahTerimaView(
                              userFinger: "iput",
                              serahTerima: "terima",
                            )));
                          }else{
                            showDialodMessage(check);
                          }
                        },
                        child: Text("IPUD"),
                        color: Colors.white,
                        shape: RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(18.0),
                            side: BorderSide(color: Colors.green)
                        ),
                      )
                  )
              ),
              SizedBox(
                height: 20,
              ),
              Center(
                  child: Container(
                      height: 60,
                      width: 200,
                      child: RaisedButton(
                        onPressed: () async{
                          var check = await CheckController().checkConnecttion();
                          if(check == "success"){
                            Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => new SerahTerimaView(
                              userFinger: "jaka",
                              serahTerima: "terima",
                            )));
                          }else{
                            showDialodMessage(check);
                          }
                        },
                        child: Text("JAKA"),
                        color: Colors.white,
                        shape: RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(18.0),
                            side: BorderSide(color: Colors.green)
                        ),
                      )
                  )
              ),
              SizedBox(
                height: 20,
              ),
              Center(
                  child: Container(
                      height: 60,
                      width: 200,
                      child: RaisedButton(
                        onPressed: () async{
                          var check = await CheckController().checkConnecttion();
                          if(check == "success"){
                            Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => new SerahTerimaView(
                              userFinger: "eri",
                              serahTerima: "terima",
                            )));
                          }else{
                            showDialodMessage(check);
                          }
                        },
                        child: Text("ERI"),
                        color: Colors.white,
                        shape: RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(18.0),
                            side: BorderSide(color: Colors.green)
                        ),
                      )
                  )
              ),
              SizedBox(
                height: 20,
              ),
              Center(
                  child: Container(
                      height: 60,
                      width: 200,
                      child: RaisedButton(
                        onPressed: () async{
                          var check = await CheckController().checkConnecttion();
                          if(check == "success"){
                            Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => new SerahTerimaView(
                              userFinger: "sudarsono",
                              serahTerima: "terima",
                            )));
                          }else{
                            showDialodMessage(check);
                          }
                        },
                        child: Text("SUDARSONO"),
                        color: Colors.white,
                        shape: RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(18.0),
                            side: BorderSide(color: Colors.green)
                        ),
                      )
                  )
              ),
              SizedBox(
                height: 20,
              ),
              Center(
                  child: Container(
                      height: 60,
                      width: 200,
                      child: RaisedButton(
                        onPressed: () async{
                          var check = await CheckController().checkConnecttion();
                          if(check == "success"){
                            Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => new SerahTerimaView(
                              userFinger: "irfan",
                              serahTerima: "terima",
                            )));
                          }else{
                            showDialodMessage(check);
                          }
                        },
                        child: Text("IRFAN"),
                        color: Colors.white,
                        shape: RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(18.0),
                            side: BorderSide(color: Colors.green)
                        ),
                      )
                  )
              ),
              SizedBox(
                height: 20,
              ),
              Center(
                  child: Container(
                      height: 60,
                      width: 200,
                      child: RaisedButton(
                        onPressed: () async{
                          var check = await CheckController().checkConnecttion();
                          if(check == "success"){
                            Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => new SerahTerimaView(
                              userFinger: "vivi",
                              serahTerima: "terima",
                            )));
                          }else{
                            showDialodMessage(check);
                          }
                        },
                        child: Text("VIVI"),
                        color: Colors.white,
                        shape: RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(18.0),
                            side: BorderSide(color: Colors.green)
                        ),
                      )
                  )
              ),
              SizedBox(
                height: 20,
              ),
              Center(
                  child: Container(
                      height: 60,
                      width: 200,
                      child: RaisedButton(
                        onPressed: () async{
                          var check = await CheckController().checkConnecttion();
                          if(check == "success"){
                            Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => new SerahTerimaView(
                              userFinger: "syaiful",
                              serahTerima: "terima",
                            )));
                          }else{
                            showDialodMessage(check);
                          }
                        },
                        child: Text("SYAIFUL"),
                        color: Colors.white,
                        shape: RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(18.0),
                            side: BorderSide(color: Colors.green)
                        ),
                      )
                  )
              ),
              SizedBox(
                height: 20,
              ),
            ],
          ),
        ),
      )
    );
  }

  void showDialodMessage(check){
    showDialog<void>(
      context: context,
      barrierDismissible: true,
      // false = user must tap button, true = tap outside dialog
      builder: (BuildContext dialogContext) {
        return AlertDialog(
          title: Text('Pesan'),
          content: Text(check),
          actions: <Widget>[
            FlatButton(
              child: Text('OK'),
              onPressed: () {
                Navigator.of(dialogContext)
                    .pop(); // Dismiss alert dialog
              },
            ),
          ],
        );
      },
    );
  }
}
