import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:local_auth/local_auth.dart';
import 'package:scanid/serahterima/controllers/ScanBarcodeBloc.dart';
import 'package:scanid/serahterima/events/SerahTerimaEvents.dart';
import 'package:scanid/serahterima/models/SampleModel.dart';

class SerahTerimaView extends StatefulWidget {
  String userFinger;
  String serahTerima;
  SerahTerimaView({this.userFinger, this.serahTerima});

  @override
  _SerahTerimaViewState createState() => _SerahTerimaViewState();
}

class _SerahTerimaViewState extends State<SerahTerimaView> {

  LocalAuthentication auth = LocalAuthentication();
  List<BiometricType> _availableBio;
  Future<void> _getAvailableBio() async{
    List<BiometricType> _available;
    try{
      _available = await auth.getAvailableBiometrics();

    } on PlatformException catch(e){
      print(e);
    }

    if(!mounted) return;

    setState(() {
      _availableBio = _available;
    });
  }

  @override
  Widget build(BuildContext context) {
    final _scanBarcodeBloc = ScanBarcodeBloc.FromUserFinger(
        userFinger: widget.userFinger
    );

    return Scaffold(
      appBar: AppBar(
        title: Text("Serah Terima Sample"),
        backgroundColor: widget.serahTerima == "serah" ? Colors.orange : Colors.green,
      ),
      body: Container(
        child: Column(
          children: <Widget>[
            SizedBox(
              height: 20,
            ),
            Center(
              child: Container(
                width: 200,
                height: 50,
                child: widget.serahTerima == "serah" ? RaisedButton(
                  onPressed: () =>  _scanBarcodeBloc.ScanBarcodeEventSink.add(ScanBarcodeEvent()),
                  child: Text("Scan Serah Sample  ",),
                  color: Colors.white,
                  shape: RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(18.0),
                      side: BorderSide(color: Colors.orange)
                  ),
                ) : RaisedButton(
                  onPressed: () =>  _scanBarcodeBloc.ScanBarcodeEventSink.add(ScanBarcodeTerimaEvent()),
                  child: Text("Scan Terima Sample"),
                  color: Colors.white,
                  shape: RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(18.0),
                      side: BorderSide(color: Colors.green)
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 20,
            ),

            Center(
              child: Text("USER FINGER : "+widget.userFinger.toUpperCase()),
            ),

            SizedBox(
              height: 20,
            ),
            Center(
              child: StreamBuilder(
                stream: _scanBarcodeBloc.ScanBarcodeStream,
                builder: (context, snapshot){
                  if(snapshot.data != null){
                    return Text("Hasil Scan : "+snapshot.data);
                  }

                  return Text("");
                },
              ),
            ),

            SizedBox(
              height: 20,
            ),
            Center(
              child: StreamBuilder(
                stream: _scanBarcodeBloc.ProsesSerahTerimaStream,
                builder: (context, AsyncSnapshot<List<SampleModel>> snap){
                  if(snap.data != null){
                    if(snap.data.length > 0){
                      String no_sample = snap.data[0].sample;
                      String message = snap.data[0].message;
//                      if(message == ""){
                        return Text("No Sample Terproses : "+no_sample+"\n USER : "+snap.data[0].user+"\n "+snap.data[0].waktu+"\n "+message);
//                      }else{
//                        return Text(message);
//                      }
                    }
                  }

                  return Text("No Sample Terproses : ");

                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
