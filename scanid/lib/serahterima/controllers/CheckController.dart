
import 'package:scanid/utils/ApiService.dart';
import 'package:http/http.dart' as http;

class CheckController{
  String getModule(){
    return "serah_terima_mobile";
  }

  Future<String> checkConnecttion () async{
    var url = ApiService.getBaseUrl() + getModule()+"/checkConnecttion";
    Map params = Map<String, String>();

    try{
      var data_api = await http.post(url, body: params).timeout(Duration(seconds: 4));

//    print("data api "+data_api.body);
      switch(data_api.statusCode){
        case 200:
          return "success";
          break;
        case 404:
          return "Halaman Tidak Ditemukan";
          break;
        case 500:
          return "Internal Server Error";
          break;
        default:
          return "Error Koneksi";
          break;
      }
    }catch(ex){
      return "Error Koneksi Timeout, Cek Kembali Jaringan Wifi Anda";
    }
  }
}