import 'dart:async';
import 'dart:convert';
import 'package:local_auth/local_auth.dart';
import 'package:scanid/serahterima/events/SerahTerimaEvents.dart';
import 'package:qrscan/qrscan.dart' as scanner;
import 'package:scanid/serahterima/models/SampleModel.dart';
import 'package:scanid/utils/ApiService.dart';
import 'package:http/http.dart' as http;

class ScanBarcodeBloc {
  String barcode;
  String userFinger;
  List<SampleModel> data;

  //Proses Scan Barcode
  final _ScanBarcodeStateController = StreamController<String>();

  StreamSink<String> get _ScanBarcode => _ScanBarcodeStateController.sink;

  Stream<String> get ScanBarcodeStream => _ScanBarcodeStateController.stream;

  final _ScanBarcodeEventController = StreamController<SerahTerimaEvents>();

  Sink<SerahTerimaEvents> get ScanBarcodeEventSink =>
      _ScanBarcodeEventController.sink;

  //Proses Serah Terima
  final _ProsesSerahTerimaStateController = StreamController<List<SampleModel>>();
  StreamSink<List<SampleModel>> get _ProsesSerahTerima => _ProsesSerahTerimaStateController.sink;

  Stream<List<SampleModel>> get ProsesSerahTerimaStream => _ProsesSerahTerimaStateController.stream;
  final _ProsesSerahTerimaController = StreamController<SerahTerimaEvents>();

  Sink<SerahTerimaEvents> get ProsesSerahTerimaEventSink =>
      _ProsesSerahTerimaController.sink;

  ScanBarcodeBloc() {
    _ScanBarcodeEventController.stream.listen(_mapEventToState);
  }


  ScanBarcodeBloc.FromUserFinger({this.userFinger}) {    
    _ScanBarcodeEventController.stream.listen(_mapEventToState);
  }

  LocalAuthentication auth = LocalAuthentication();


  String getModule(){
    return "serah_terima_mobile";
  }

  void _prosesSerahTerima(String serah_terima) async {
//    if(event is ProsesSerahTerimaEvent){
      List<SampleModel> data_result = [];

//      var url = ApiService.getBaseUrl() + getModule()+"/prosesSerahTerima";
      var url = ApiService.getBaseUrl() + getModule()+"/execSerahTerima";
      Map params = Map<String, String>();
      params["user"] = this.userFinger;
      params["serah_terima"] = serah_terima;
     params["sample"] = barcode;
//       params["sample"] = "2020A01625";

//      print("url "+url);
      var data_api = await http.post(url, body: params);

       print("data api "+data_api.body);
      switch(data_api.statusCode){
        case 200:
          var _data_json = jsonDecode(data_api.body);
          List data_sample = _data_json["data"];

          if (data_sample.length > 0) {
            for (var val in data_sample) {
              data_result.add(SampleModel(
                  sample: val["sample"],
                  user: val["user"],
                  waktu: val["waktu"],
                  message: val["message"]
              ));
            }
          }
          break;
        case 404:
          print("Halaman Tidak Ditemukan "+url.toString());
          print("url "+url);
          break;
        case 500:
          print("Internal Server Error");
          break;
      }

      data = data_result;
//    }
//
//    print("data hasil "+data.toString());
    _ProsesSerahTerima.add(data);
  }

  void _mapEventToState(SerahTerimaEvents event) async {
    if(event is ScanBarcodeEvent){
//      String cameraScanResult = await scanner.scan();
//      barcode = cameraScanResult;

//      if(barcode != "" && barcode != null){
        openAuthFinger("serah");
//      }
    }

    if(event is ScanBarcodeTerimaEvent){
//      String cameraScanResult = await scanner.scan();
//      barcode = cameraScanResult;

//      if(barcode != "" && barcode != null){
        openAuthFinger("terima");
//      }
    }

    _ScanBarcode.add(barcode);
  }

  void openAuthFinger(String serah_terima) async{
    _prosesSerahTerima(serah_terima);

//    try{
//      bool _authenti = await auth.authenticateWithBiometrics(
//          localizedReason: "Scan Yout Finger to Authentificate",
//          useErrorDialogs: false,
//          stickyAuth: false,
//      );
//
//
//      if(_authenti){
//        print("E "+_authenti.hashCode.toString());
//        _prosesSerahTerima(serah_terima);
//      }else{
//        print("Finger Tidak Valid");
//      }
//      print("HASIL FINGER "+_authenti.toString());
//    } catch(e){
//      print(e);
//    }
  }
}
