import 'package:flutter/material.dart';
import 'package:scanid/scan/bloc/scan.bloc.dart';
import 'package:scanid/scan/views/ScanView.dart';
// import 'package:scanid/login/views/LoginView.dart';
// import 'package:scanid/scan/views/ScanView.dart';
import 'package:scanid/serahterima/views/ListUserViews.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
// import 'package:scanid/serahterima/views/SerahTerimaView.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<ScanCameraBloc>(
            create: (BuildContext context) => ScanCameraBloc())
      ],
      child: MaterialApp(
        title: 'Feedmill',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
         home: ListUserView(),
       // home: ScanView(),
      ),
    );
  }
}
